# Notices for Eclipse APP4MC

This content is produced and maintained by the Eclipse APP4MC project.

* Project home: https://projects.eclipse.org/projects/automotive.app4mc

## Trademarks

APP4MC™ is a trademark of the Eclipse Foundation.

## Copyright

All content is the property of the respective authors or their employers. For
more information regarding authorship of content, please consult the listed
source code repository logs.

## Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
https://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0

## Source Code

The project maintains the following source code repositories:

* https://gitlab.eclipse.org/eclipse/app4mc/org.eclipse.app4mc.git
* https://gitlab.eclipse.org/eclipse/app4mc/org.eclipse.app4mc.addon.migration.git
* https://gitlab.eclipse.org/eclipse/app4mc/org.eclipse.app4mc.addon.transformation.git
* https://gitlab.eclipse.org/eclipse/app4mc/org.eclipse.app4mc.cloud.git
* https://gitlab.eclipse.org/eclipse/app4mc/org.eclipse.app4mc.examples.git
* https://gitlab.eclipse.org/eclipse/app4mc/org.eclipse.app4mc.releng.git
* https://gitlab.eclipse.org/eclipse/app4mc/org.eclipse.app4mc.rtclib.git
* https://gitlab.eclipse.org/eclipse/app4mc/org.eclipse.app4mc.tools.git
* https://gitlab.eclipse.org/eclipse/app4mc/org.eclipse.app4mc.tools.rtc.git
* https://gitlab.eclipse.org/eclipse/app4mc/org.eclipse.app4mc.tools.simulation.git
* https://gitlab.eclipse.org/eclipse/app4mc/org.eclipse.app4mc.tools.simulation.examples.git
* https://gitlab.eclipse.org/eclipse/app4mc/org.eclipse.app4mc.website.source.git
* https://gitlab.eclipse.org/eclipse/app4mc/org.eclipse.app4mc.website.git

## Cryptography

Content may contain encryption software. The country in which you are currently
may have restrictions on the import, possession, and use, and/or re-export to
another country, of encryption software. BEFORE using any encryption software,
please check the country's laws, regulations and policies concerning the import,
possession, or use, and re-export of encryption software, to see if this is
permitted.
