/* Point implementation.

Copyright (c) 2004-2006 Computer Engineering and Networks Laboratory (TIK), 
Swiss Federal Institute of Technology (ETH) Zurich.
All rights reserved.
Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH 
BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR 
CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS 
DOCUMENTATION, EVEN IF THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) 
ZURICH HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH SPECIFICALLY 
DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE SWISS 
FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH  HAS NO OBLIGATION TO 
PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

                                       RTC_COPYRIGHT_VERSION_1
                                       COPYRIGHTENDKEY

@ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
@AcceptedRating 
@JUnit 			none (wandeler@tik.ee.ethz.ch)

Created: 28.10.2005 by Ernesto Wandeler
*/
package org.eclipse.app4mc.rtclib.util;


/**
 * <code>Point</code> represents a point in a cartesian coordinate system.
 * 
 * @author         Ernesto Wandeler
 * @version        $Id: Point.java 359 2006-10-05 13:48:28Z wandeler $
 * @since          
 * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
 * @AcceptedRating none
 * @JUnitRating    none (wandeler@tik.ee.thz.ch)
 */
public class Point {

	/**
	 * Creates a Point with the given data.
     * 
	 * @param x the x-value of the created Point.
	 * @param y the y-value of the created Point.
	 */
	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Returns the x-value of this Point.
     * 
	 * @return the x-value of this Point.
	 */
	public double x() {
		return x;
	}
	
	/**
	 * Returns the y-value of this Point.
     * 
	 * @return the y-value of this Point.
	 */
	public double y() {
		return y;
	}
	
	/**
	 * Sets the x-value of this Point.
     * 
	 * @param x set the x-value of this Point.
	 */
	public void setX(double x) {
		this.x = x;
	}
	
	/**
	 * Sets the y-value of this Point.
     * 
	 * @param y set the y-value of this Point.
	 */
	public void setY(double y) {
		this.y = y;
	}
	
    /** 
     * Indicates whether some other Point is equal to this one.
     * 
     * @param obj the Point to compare this one with.
     * @return <code>true</code> if this Point equals object, 
     * <code>false</code> otherwise.
     */
	public boolean equals(Object obj) {
		if (obj == null) return false;
        Point p = (Point) obj;
        if (DoubleMath.neq(p.x(), this.x())) return false;
        if (DoubleMath.neq(p.y(), this.y())) return false;
        return true;		
	}
	
	private double x;
	private double y;
}
