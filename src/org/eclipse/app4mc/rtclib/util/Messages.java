/* Messages implementation.

Copyright (c) 2004-2006 Computer Engineering and Networks Laboratory (TIK), 
Swiss Federal Institute of Technology (ETH) Zurich.
All rights reserved.
Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH 
BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR 
CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS 
DOCUMENTATION, EVEN IF THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) 
ZURICH HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH SPECIFICALLY 
DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE SWISS 
FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH  HAS NO OBLIGATION TO 
PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

                                       RTC_COPYRIGHT_VERSION_1
                                       COPYRIGHTENDKEY

@ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
@AcceptedRating 
@JUnit			none (wandeler@tik.ee.ethz.ch)

Created: 18.02.2005 by Ernesto Wandeler
*/
package org.eclipse.app4mc.rtclib.util;


/**
 * <code>Messages</code> provides a number of user interface messages.
 * 
 * @author         Ernesto Wandeler
 * @version        $Id: Messages.java 942 2010-07-26 11:39:35Z psimon $
 * @since          
 * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
 * @AcceptedRating none
 * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
 */
public class Messages {
	public static String RTC_VERSION = "@version@";
	// @version@ is replaced by the ant tool (build.xml):
	// <filter token="version" value="${TODAY_UK} #${toolbox.version}.${build.number}"/>
	
    public static String ILLEGAL_ARGUMENT_ORDER_APER = 
        	"CurveSegments in aperiodicPart are not strictly increasing.";
    public static String ILLEGAL_ARGUMENT_START_APER = 
    		"The aperiodicPart does not start at 0.0";
    public static String ILLEGAL_ARGUMENT_ORDER_PER = 
    		"CurveSegments in periodicPart are not strictly increasing.";
    public static String ILLEGAL_ARGUMENT_START_PER = 
			"The periodicPart does not start at 0.0";
    public static String ILLEGAL_ARGUMENT_BOTH_NULL = 
			"Both aperiodicPart and periodicPart are null";
    public static String LIMITEDSEGMENT_ENDX_LT_X = 
        	"xEnd must be greateer or equal than x";
    public static String SEGMENT_SCALEX_FACTOR_LEQ_0 = 
            "factor must be greater or equal 0";  
    public static String SCALEX_FACTOR_PERIODOC =
            "(factor * period) must be an integer";
    public static String SEGMENT_LIST_NOT_INVERTABLE = 
            "This SegmentList cannot be inverted.";
    public static String SEGMENT_LIST_A_NOT_INVERTABLE = 
            "SegmentList A cannot be inverted.";
    public static String SEGMENT_LIST_B_NOT_INVERTABLE = 
            "SegmentList B cannot be inverted.";
    public static String PERIOD_NOT_INVERTABLE = 
        	"pdy must be integer.";
    public static String MAXHDIST_UNDEFINED = 
            "The maximum vertical distance is not defined for the passed curves:";
    public static String SIMULINK_BUFFER_OVERFLOW = 
            "Simulink signal buffer overflow! Minimum size required: ";
    public static String NON_WSI_CURVE = 
        	"This method requires a wide sense increasing Curve as input.";
    public static String VALUE_UNDEFINED = 
        "The value is not defined by this Curve.";    
    public static String ILLEGAL_ARGUMENT_SAMPLE_ARRAY_TOO_SMALL = 
    	    "Not enough samples for curve specification.";
    public static String PERIOD_ZERO = 
	        "Curve with period 0. Increase number of samples.";
         
}



