/* Vector implementation.

Copyright (c) 2004-2006 Computer Engineering and Networks Laboratory (TIK), 
Swiss Federal Institute of Technology (ETH) Zurich.
All rights reserved.
Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH 
BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR 
CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS 
DOCUMENTATION, EVEN IF THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) 
ZURICH HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH SPECIFICALLY 
DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE SWISS 
FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH  HAS NO OBLIGATION TO 
PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

                                       RTC_COPYRIGHT_VERSION_1
                                       COPYRIGHTENDKEY

@ProposedRating yellow (wandeler@tik.ee.ethz.ch)
@AcceptedRating 
@JUnit			yellow (wandeler@tik.ee.ethz.ch)

Created: 28.10.2005 by Ernesto Wandeler
*/
package org.eclipse.app4mc.rtclib.util;


/**
 * <code>Point</code> represents a vector in a cartesian coordinate system.
 * 
 * @author         Ernesto Wandeler
 * @version        $Id: Vector.java 918 2008-12-23 11:38:46Z nikolays $
 * @since          
 * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
 * @AcceptedRating none
 * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
 */
public class Vector {

    /**
     * Creates a Vector that starts and ends at the given start and end 
     * coordinates.
     * 
     * @param xStart the start x-coordinate.
     * @param yStart the start y-coordinate.
     * @param xEnd the end x-coordinate.
     * @param yEnd the end y-coordinate.
     */
    public Vector(double xStart, double yStart, double xEnd, double yEnd) {
        this.xStart = xStart;
        this.yStart = yStart;
        this.xEnd = xEnd;
        this.yEnd = yEnd;
    }
    
    /**
     * Creates a Vector that starts and ends at the given Points.
     * 
     * @param start the start Point.
     * @param end the end Point.
     */
    public Vector(Point start, Point end) {
        this.xStart = start.x();
        this.yStart = start.y();
        this.xEnd = end.x();
        this.yEnd = end.y();
    } 
    
    /**
     * Returns the start x-coordinate.
     * 
     * @return the start x-coordinate.
     */
    public double xStart() {
        return xStart;
    }
    
    /**
     * Returns the start y-coordinate.
     * 
     * @return the start y-coordinate.
     */
    public double yStart() {
        return yStart;
    }

    /**
     * Returns the end x-coordinate.
     * 
     * @return the end x-coordinate.
     */
    public double xEnd() {
        return xEnd;
    }

    /**
     * Returns the end y-coordinate.
     * 
     * @return the end y-coordinate.
     */
    public double yEnd() {
        return yEnd;
    }
    
    /**
     * Returns the length of this Vector.
     * 
     * @return the length of this Vector.
     */
    public double length() {
        if ((Double.isInfinite(xStart) || Double.isInfinite(xEnd)) && 
                (Double.isInfinite(yStart) || Double.isInfinite(yEnd))) {
            return Double.POSITIVE_INFINITY;
        } else {
            return Math.sqrt(Math.pow((xEnd - xStart), 2) 
                    + Math.pow((yEnd - yStart), 2));
        }
    }
    
    private double xStart;
    private double yStart;
    private double xEnd;
    private double yEnd;
}
