/* SegmentListIterator implementation.

Copyright (c) 2004-2006 Computer Engineering and Networks Laboratory (TIK), 
Swiss Federal Institute of Technology (ETH) Zurich.
All rights reserved.
Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH 
BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR 
CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS 
DOCUMENTATION, EVEN IF THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) 
ZURICH HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH SPECIFICALLY 
DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE SWISS 
FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH  HAS NO OBLIGATION TO 
PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

                                       RTC_COPYRIGHT_VERSION_1
                                       COPYRIGHTENDKEY

@ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
@AcceptedRating 
@JUnitTesting   Yellow (wandeler@tik.ee.ethz.ch)

Created: 31.10.2005 by Ernesto Wandeler
*/

package org.eclipse.app4mc.rtclib;

import org.eclipse.app4mc.rtclib.util.DoubleMath;


/**
 * <code>SegmentListIterator</code> provides an iterator that iterates over
 * the single curve segments of a segment list.
 * 
 * @author         Ernesto Wandeler
 * @version        $Id: SegmentListIterator.java 918 2008-12-23 11:38:46Z nikolays $
 * @since          
 * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
 * @AcceptedRating none
 * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
 */
public class SegmentListIterator {
    
    /**
     * Creates a SegmentListIterator that iterates over all Segments in
     * the passed SegmentList, up to x <= xMax. 
     * 
     * @param list the SegmentList to iterate over.
     * @param xMax the maximum value up to which to iterate.
     */
    public SegmentListIterator(SegmentList list, double xMax) {
        if (list != null) {
            this.list = list;
            numOfSeg = list.size();
            pointer = 0;
            if (numOfSeg > pointer) {
                nextSeg = (Segment)list.get(pointer);
            } else {
                nextSeg = null;
            }
            prevSeg = null;
            seg = null;
            xStart = Double.NaN;
            xEnd = Double.NaN;
            pointer = pointer + 1;
            if (Double.isNaN(xMax)) {
                this.xMax = Double.POSITIVE_INFINITY;
            } else if (DoubleMath.lt(xMax, 0.0)) {
                this.list = null;
            } else {
                this.xMax = xMax;
            }
        }
    }
    
    /**
     * Creates a SegmentListIterator that iterates over all Segments in
     * the passed SegmentList. 
     * 
     * @param list the SegmentList to iterate over.
     */
    public SegmentListIterator(SegmentList list) {
        this(list, Double.NaN);
    }
    
	/**
	 * Advances this Iterator to the next Segment in the SegmentList and returns 
     * true if the end of the SegmentList is not reached yet.
     *
	 * @return true, if the end of the SegmentList is not reached yet.
	 */
	public boolean next() {
        init = true;
        if (list == null) {
            return false;
        }
        if (nextSeg == null || DoubleMath.gt(nextSeg.x(), xMax)) {
            return false;
        }
        if (pointer < numOfSeg) {
            prevSeg = seg;
            seg = nextSeg;
            nextSeg = (Segment) list.get(pointer);
            pointer = pointer + 1;
            xStart = seg.x();
            yStart = seg.y();
            xEnd = Math.min(nextSeg.x(), xMax);
            yEnd = seg.yAt(xEnd);
            return true;
        } else if (pointer == numOfSeg) {
            prevSeg = seg;
            seg = nextSeg;
            nextSeg = null;
            pointer = pointer + 1;
            xStart = seg.x();
            yStart = seg.y();
            xEnd = Math.min(Double.POSITIVE_INFINITY, xMax);
            yEnd = seg.yAt(xEnd);
            return true;
        } else {
            return false;
        }
	}
	
	/**
     * Moves this Iterator to the previous Segment in the SegmentList and 
     * returns true if the start of the SegmentList is not reached yet.
     * 
	 * @return true, if the Iterator did not reach its start yet.
	 */
	public boolean prev() {
        if (!init) {
            this.next();
            init = true;
        }
        if (!initReturn) {
            initReturn = true;
            return true;
        }
        if (list == null) {
            return false;
        }
        if (pointer > 3) {
            nextSeg = seg;
            seg = prevSeg;
            prevSeg = (Segment) list.get(pointer - 4);
            pointer = pointer - 1;
            xStart = seg.x();
            yStart = seg.y();
            xEnd = nextSeg.x();
            yEnd = seg.yAt(xEnd);
            return true;
        } else if (pointer == 3) {
            nextSeg = seg;
            seg = prevSeg;
            prevSeg = null;
            pointer = pointer - 1;
            xStart = seg.x();
            yStart = seg.y();
            xEnd = nextSeg.x();
            yEnd = seg.yAt(xEnd);
            return true;
        } else {
            return false;
        }   
	}
	
	/**
	 * Resets this SegmentListIterator.
	 */
	public void reset() {
        if (list != null) {
            pointer = 0;
            if (numOfSeg > pointer) {
                nextSeg = (Segment)list.get(pointer);
            } else {
                nextSeg = null;
            }
            prevSeg = null;
            seg = null;
            xStart = Double.NaN;
            xEnd = Double.NaN;
            pointer = pointer + 1;
        }
        init = false;
	}
    
    /**
     * Resets this SegmentListIterator to the end. After a call to this method
     * the first call to prev() will move this SegmentListIterator to the
     * last Segment in the Segment list.
     */
    public void resetToEnd() {
        if (list != null) {
            while(next()) {
                // nop
            }
            initReturn = false;
        } else {
            init = false;
        }
        return;
    }
	
	/**
	 * Returns the current Segment.
     * 
	 * @return the current Segment.
	 */
	public Segment segment() {
        if (!init) throw new IllegalStateException();
	    return seg;
	}
    
	/**
	 * Returns the x-value of the start of the current Segment.
     * 
	 * @return the x-value of the start of the current Segment.
	 */
	public double xStart() {
        if (!init) throw new IllegalStateException();
	    return xStart;
	}
	
	/**
	 * Returns the x-value of the end of the current Segment.
     * 
	 * @return the x-value of the end of the current Segment.
	 */
	public double xEnd() {
        if (!init) throw new IllegalStateException();
	    return xEnd;
	}
	
	/**
	 * Returns the y-value of the start of the current Segment.
     * 
	 * @return the y-value of the start of the current Segment.
	 */
	public double yStart() {
        if (!init) throw new IllegalStateException();
	    return yStart;
	}
	
	/**
	 * Returns the y-value of the end of the current Segment.
     * 
	 * @return the y-value of the end of the current Segment.
	 */
	public double yEnd() {
        if (!init) throw new IllegalStateException();
	    return yEnd;
	}
	
	/**
	 * Returns the slope of the current Segment.
     * 
	 * @return the slope of the current Segment.
	 */
	public double s() {
        if (!init) throw new IllegalStateException();
	    return seg.s;
	}
	
	/**
	 * Returns the number of Segments of the SegmentList.
     * 
	 * @return the number of Segments of the SegmentList.
	 */
	public int size() {
        return list.size();
	}

    /**
     * Returns the index of the current Segment.
     * 
     * @return the index of the current Segment.
     */
    public int index() {
        if (!init) throw new IllegalStateException();
        return pointer - 2;
    }
    
    private SegmentList list = null;
	private int numOfSeg, pointer;
	private Segment seg, nextSeg, prevSeg;
	private double xStart, xEnd;
	private double yStart, yEnd;
    private boolean init = false, initReturn = true;
    private double xMax;
 }