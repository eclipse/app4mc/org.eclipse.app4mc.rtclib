/* PeriodicPart implementation.

Copyright (c) 2004-2006 Computer Engineering and Networks Laboratory (TIK), 
Swiss Federal Institute of Technology (ETH) Zurich.
All rights reserved.
Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH 
BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR 
CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS 
DOCUMENTATION, EVEN IF THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) 
ZURICH HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH SPECIFICALLY 
DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE SWISS 
FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH  HAS NO OBLIGATION TO 
PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

                                       RTC_COPYRIGHT_VERSION_1
                                       COPYRIGHTENDKEY

@ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
@AcceptedRating 
@JUnit		    Yellow (wandeler@tik.ee.ethz.ch) 

Created: 01.11.2005 by Ernesto Wandeler
*/

/*
 * TODO force that all curves behave the same regarding free degree between
 * 		periodicStartY and the y-value of all curve segments.
 */

package org.eclipse.app4mc.rtclib;

import org.eclipse.app4mc.rtclib.util.DoubleMath;
import org.eclipse.app4mc.rtclib.util.MathUtil;
import org.eclipse.app4mc.rtclib.util.Messages;
import java.io.Serializable;


/**
 * <code>AperiodicPartr</code> represents the periodic part of a curve.
 * 
 * @author         Ernesto Wandeler
 * @version        $Id: PeriodicPart.java 968 2015-09-11 11:37:36Z thiele $
 * @since          
 * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
 * @AcceptedRating none
 * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
 */
public class PeriodicPart implements Cloneable, Serializable {
			
    // is set in the build.xml (replace while copying files)
	// private static final long serialVersionUID = Integer.parseInt("@serialVersionUID@");	
	private static final long serialVersionUID = 1598352218976515684L;

    protected PeriodicPart(SegmentList per, double px0, double py0, long period, 
            double pdx, double pdy, double pds, double pyMin, double pyMax) {
        this.segments = per;
        this.px0 = px0;
        this.py0 = py0;
        this.period = period;
        this.pdx = pdx;
        this.pdy = pdy;
        this.pds = pds;
        this.pyMin = pyMin;
        this.pyMax = pyMax;
    }

	/**
     * Constructs a PerdiodicPart from the passed arguments. pdy may be NaN, 
     * then pdy is computed to equal the y-coordinate of the SegmentList at pdx.
     * 
	 * @param segmentList the SegmentList.
	 * @param px0 the x-coordinate where this PeriodicPart starts.
	 * @param py0 the y-coordinate where this PeriodicPart starts.
	 * @param period the period of this PeriodicPart.
	 * @param pdy the periodic offset of this PeriodicPart.
	 */
	public PeriodicPart(SegmentList segmentList, 
			double px0, double py0, long period, double pdy) {
        if (segmentList == null) throw new IllegalArgumentException();
        if (!segmentList.checkOrder()) throw new IllegalArgumentException(
                Messages.ILLEGAL_ARGUMENT_ORDER_PER);
        if (DoubleMath.neq(segmentList.segment(0).x(), 0.0)) 
            throw new IllegalArgumentException(
                    Messages.ILLEGAL_ARGUMENT_START_PER);
        segmentList.trimLT(period);
        this.segments = segmentList;
        if (Double.isNaN(pdy)) {
            pdy = this.segments.lastSegment().yAt(period);              
        } 
        this.px0 = px0;
        this.py0 = py0;
        this.period = period;
        this.pdy = pdy;
        normalizeCoordinates();
        update();
	}

    /**
     * Constructs a PeriodicPart from a double[][] of the form
     * [[x1 y1 s1];[x2 y2 s2];[x3 y3 s3]] and the additional parameters.
     * This constructor is intended for use with Matlab.
     * 
     * @param segmentList the double[][]
     */
	public PeriodicPart(double[][] segmentList, 
            double px0, double py0, long period, double pdy) { 
        this(new SegmentList(segmentList), px0, py0, period, pdy);
    }
    
    /**
     * Constructs a PeriodicPart from a String of the form 
     * "[[x1 y1 s1];[x2 y2 s2];[x3 y3 s3]],px0,py0,period,pdy".
     * 
     * @param str the String of form 
     * "[[x1 y1 s1];[x2 y2 s2];[x3 y3 s3]],px0,py0,period,pdy"
     */
    public PeriodicPart(String str) {
        String[] elem = str.split(",");
        PeriodicPart temp = new PeriodicPart(new SegmentList(elem[0].trim()),
                Double.valueOf(elem[1].trim()).doubleValue(),
                Double.valueOf(elem[2].trim()).doubleValue(),
                Long.valueOf(elem[3].trim()).longValue(),
                Double.valueOf(elem[4].trim()).doubleValue());
        this.segments = temp.segments(); 
        this.px0 = temp.px0(); 
        this.py0 = temp.py0();
        this.period = temp.period(); 
        this.pdx = temp.pdx();
        this.pdy = temp.pdy();
        this.pds = temp.pds();
        this.pyMin = temp.pyMin();
        this.pyMax = temp.pyMax();
    }
	
    /**
     * Returns the segments of this PeriodicPart as a SegmentList.
     * 
     * @return the segments of this PeriodicPart as a SegmentList.
     */
    public SegmentList segments() {
        return this.segments;
    }


    
    /**
     * Returns the period of this PeriodicPart.
     * 
     * @return the period of this PeriodicPart.
     */
    public long period() {
        return this.period;
    }

    /**
     * Returns the x-coordinate where this PeriodicPart starts.
     * 
     * @return the x-coordinate where this PeriodicPart starts.
     */
    public double px0() {        
        return this.px0;
    }

    /**
     * Returns the y-coordinate where this PeriodicPart starts.
     * 
     * @return the y-coordinate where this PeriodicPart starts.
     */
    public double py0() {
        return this.py0;
    }

    /**
     * Returns the delta on the x-axis between two consecutive repetitions
     * of this PeriodicPart, i.e. the period itself. 
     * 
     * @return the delta on the x-axis between two consecutive repetitions
     * of this PeriodicPart, i.e. the period itself. 
     */
    public double pdx() {
        return this.pdx;
    }

    /**
     * Returns the delta on the y-axis between two consecutive repetitions
     * of this PeriodicPart, i.e. the offset. 
     * 
     * @return the delta on the y-axis between two consecutive repetitions
     * of this PeriodicPart, i.e. the offset. .
     */
    public double pdy() {
        return this.pdy;
    }

    /**
     * Returns the overall slope of this PeriodicPart: pds = pdy / pdx. 
     * 
     * @return the overall slope of this PeriodicPart.
     */
    public double pds() {
        return this.pds;
    }

    /**
     * Returns the minimum y-value of this PeriodicPart within x = [0,pdx] 
     * in the relative coordinate system with origin (px0/py0).
     * 
     * @return the minimum y-value of this PeriodicPart.
     */
    public double pyMin() {
        return this.pyMin;
    }

    /**
     * Returns the maximum y-value of this PeriodicPart within x = [0,pdx] 
     * in the relative coordinate system with origin (px0/py0). 
     * 
     * @return the maximum y-value of this PeriodicPart.
     */
    public double pyMax() {
        return this.pyMax;
    }

    /**
     * Scale this PeriodicPart along the x-axis. The result of the 
     * multiplication (factor * pdx) must be an integer!
     * 
     * @param factor the factor to scale this PeriodicPart.
     */
    public void scaleX(double factor) {
        if (!MathUtil.isFullNumber(factor*pdx)) {
            throw new IllegalArgumentException(Messages.SCALEX_FACTOR_PERIODOC);
        }
        segments.scaleX(factor);
        this.period *= factor;
        this.px0 *= factor;
        this.pdx *= factor;
        this.pds /= factor;
    }

    /**
     * Scale this PeriodicPart along the y-axis.
     * 
     * @param factor the factor to scale this PeriodicPart.
     */
    public void scaleY(double factor) {
        segments.scaleY(factor);
        this.py0 *= factor;
        this.pdy *= factor;
        this.pds *= factor;
        this.pyMin *= factor;
        this.pyMax *= factor;
        if (factor < 0) {
            double temp = this.pyMax;
            this.pyMax = this.pyMin;
            this.pyMin = temp;
        }  
    }

    /**
     * Move this PeriodicPart by (dx,dy). If dx is negative, it may happen that
     * px0 becomes negative. In this case, px0 is set to the start point of the
     * first periodic repetition that starts at a positive x-coordinate. If this
     * first positive x-coordinate does not equal 0.0, then an AperiodicPart is
     * returned that represents the fractional part from 0.0 up to the new px0.
     * In any other case, null is returned. 
     * 
     * @param dx the x-value by which this PeriodicPart is moved.
     * @param dy the y-value by which this PeriodicPart is moved.
     */
    public AperiodicPart move(double dx, double dy) {
        this.px0 += dx;
        this.py0 += dy;

        if (DoubleMath.lt(this.px0, 0.0)) {
            double numOfNegPeriods = (-this.px0 / this.pdx);
            if (MathUtil.isFullNumber(numOfNegPeriods)) {
                this.px0 = 0.0;
                this.py0 = this.py0 + (numOfNegPeriods * this.pdy);
                return null;
            } else {
                this.px0 = this.px0 + (Math.ceil(numOfNegPeriods) * this.pdx);
                this.py0 = this.py0 + (Math.ceil(numOfNegPeriods) * this.pdy);
                SegmentList aperiodic = (SegmentList)this.segments.clone();
                aperiodic.move(this.px0 - this.pdx, this.py0 - this.pdy);
                int i = aperiodic.size() - 1;
                boolean reachedNegative = false;
                Segment seg = null;
                while(i >= 0) {
                    if (!reachedNegative) {
                        seg = aperiodic.segment(i);
                        if (DoubleMath.eq(seg.x(), 0.0)) {
                            reachedNegative = true;
                        } else if (DoubleMath.lt(seg.x(), 0.0)) {
                            seg.setY(seg.yAt(0.0));
                            seg.setX(0.0);
                            reachedNegative = true;
                        }                                  
                    } else {
                        aperiodic.remove(i);
                    }
                    i = i - 1;
                }
                return new AperiodicPart(aperiodic);
            }
        } else {
            return null;
        }
    }
    
	/**
     * Round all points that define this PeriodicPart.
     * This includes all start points of all segments, all slopes, 
     * as well as px0, py0, pdy.
     * All related values such as slopes and pds, pyMin and pyMax are new 
     * calculated.
	 */
	public void round() {
        segments.round();
        this.px0 = MathUtil.round(this.px0);
        this.py0 = MathUtil.round(this.py0);
        this.pdy = MathUtil.round(this.pdy);
        this.update();
	}
	
    /** 
     * Indicates whether some other PeriodicPart is equal to this one.
     * 
     * @param object the PeriodicPart to compare this one with.
     * @return <code>true</code> if this PeriodicPart equals object, 
     * <code>false</code> otherwise.
     */
    public boolean equals(Object object){
        if (object == null) return false;
        PeriodicPart part = (PeriodicPart) object;
        if (this.period != part.period()) return false;
        if (DoubleMath.neq(this.pdx, part.pdx())) return false;
        if (DoubleMath.neq(this.pdy, part.pdy())) return false;
        if (DoubleMath.neq(this.pds, part.pds())) return false;
        if (DoubleMath.neq(this.px0, part.px0())) return false;
        if (DoubleMath.neq(this.py0, part.py0())) return false;
        if (DoubleMath.neq(this.pyMin, part.pyMin())) return false;
        if (DoubleMath.neq(this.pyMax, part.pyMax())) return false;
        if (!this.segments.equals(part.segments())) return false;
        return true;    
    }

    /** 
     * Creates a deep copy of this PeriodicPart.
     * 
     * @return a deep copy of this PeriodicPart.
     */
	public PeriodicPart clone() {
	    return new PeriodicPart((SegmentList)this.segments.clone(), 
                this.px0, this.py0, this.period, this.pdy);
	}
	
    /** 
     * Returns a String representation of this PeriodicPart.
     * 
     * @return a String representation of this PeriodicPart.
     */
	public String toString() {
        String str = "";
        str += "  PeriodicPart   = " + this.segments.toString() + " \n";
        str += "   (px0,py0)     = (" + this.px0 + "," + this.py0 + ")\n";
        str += "   (pdx,pdy,pds) = (" + this.pdx + "," + this.pdy + "," + this.pds + ")\n";
        str += "   (pyMin,pyMax) = (" + this.pyMin + "," + this.pyMax + ")\n";
        return str;
	}

	/**
	 * Returns the data of this PeriodicPart in a textual form for export. 
     * 
	 * @return the data of this PeriodicPart in a textual form for export.
	 */
	public String toExportString() {
        String str = "";
        str += this.segments.toMatlabString();
        str += ", " + this.px0 + ", " + this.py0;
        str += ", " + this.period + ", " + this.pdy;
        return str;
	}
		
    /**
     * Updates the values of [pdx, pds, pyMin, pyMax] as a function of
     * [perdiod, pdy, segments].
     */
    protected void update() {
        this.pdx = (double)this.period;
        this.pds = this.pdy / this.pdx;
        this.pyMin = this.segments.min(this.pdx);
        this.pyMax = this.segments.max(this.pdx);
    }
    
    /**
     * Normalizes the relative coordinate system of this PeriodicPart, such
     * that the first segment of this PeriodicPart starts at (0,0) in the
     * relative coordinate system, i.e. at (px0,py0) in the absolute
     * coordinate system. 
     */
    protected void normalizeCoordinates() {
        if (this.segments.size() > 0) {
            double y0rel = this.segments.segment(0).y();
            if (DoubleMath.neq(y0rel, 0.0)) {
                this.segments.move(0.0, -y0rel);
                this.py0 += y0rel;
            }
        }
    }
    
    private SegmentList segments;
    private long period;
    private double px0;
    private double py0;
    private double pdx;
    private double pdy;
    private double pds;
    private double pyMin;
    private double pyMax;

}
