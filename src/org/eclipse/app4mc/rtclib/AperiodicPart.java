/* AperiodicPart implementation.

Copyright (c) 2004-2006 Computer Engineering and Networks Laboratory (TIK), 
Swiss Federal Institute of Technology (ETH) Zurich.
All rights reserved.
Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH 
BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR 
CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS 
DOCUMENTATION, EVEN IF THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) 
ZURICH HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH SPECIFICALLY 
DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE SWISS 
FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH  HAS NO OBLIGATION TO 
PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

                                       RTC_COPYRIGHT_VERSION_1
                                       COPYRIGHTENDKEY

@ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
@AcceptedRating 
@JUnit			Yellow (wandeler@tik.ee.ethz.ch) 

Created: 31.10.2005 by Ernesto Wandeler
*/

package org.eclipse.app4mc.rtclib;

import org.eclipse.app4mc.rtclib.util.DoubleMath;
import org.eclipse.app4mc.rtclib.util.Messages;
import java.io.Serializable;

/**
 * <code>AperiodicPart</code> represents the aperiodic part of a curve.
 * 
 * @author         Ernesto Wandeler
 * @version        $Id: AperiodicPart.java 968 2015-09-11 11:37:36Z thiele $
 * @since          
 * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
 * @AcceptedRating none
 * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
 */
public class AperiodicPart implements Cloneable, Serializable {
			
    // is set in the build.xml (replace while copying files)
//	private static final long serialVersionUID = Integer.parseInt("@serialVersionUID@");
	private static final long serialVersionUID = 1598352218976515684L;
	
	///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////


    /**
     * Constructs an AperiodicPart from the passed SegmentList, if 
     * noCheck is true, no checks on the input data will be performed! 
     * 
     * @param segmentList the SegmentList.
     * @param noChecks indicates whether the input data should be checked or not.
     */ 
    protected AperiodicPart(SegmentList segmentList, boolean noChecks) {
        if (noChecks) {
            this.segments = segmentList;
        } else {
            if (segmentList == null) throw new IllegalArgumentException();
            if (!segmentList.checkOrder()) throw new IllegalArgumentException(
                    Messages.ILLEGAL_ARGUMENT_ORDER_APER);
            if (DoubleMath.neq(segmentList.segment(0).x(), 0.0)) 
                throw new IllegalArgumentException(
                        Messages.ILLEGAL_ARGUMENT_START_APER);
            this.segments = segmentList;   
        }
    }
    
	/**
	 * Constructs an AperiodicPart from the passed SegmentList.
     * 
     * @param segmentList the SegmentList.
	 */	
	public AperiodicPart(SegmentList segmentList) {
        if (segmentList == null) throw new IllegalArgumentException();
        if (!segmentList.checkOrder()) throw new IllegalArgumentException(
                Messages.ILLEGAL_ARGUMENT_ORDER_APER);
        if (DoubleMath.neq(segmentList.segment(0).x(), 0.0)) 
            throw new IllegalArgumentException(
                    Messages.ILLEGAL_ARGUMENT_START_APER);
		this.segments = segmentList;
	}

	/**
     * Constructs an AperiodicPart from a double[][] of the form
     * [[x1 y1 s1];[x2 y2 s2];[x3 y3 s3]]
	 * This constructor is intended for use with Matlab.
     * 
     * @param segmentList the double[][]
	 */
	public AperiodicPart(double[][] segmentList) {
        this.segments = new SegmentList(segmentList);
	}
    
    /**
     * Constructs an AperiodicPart from a String of the form 
     * "[[x1 y1 s1];[x2 y2 s2];[x3 y3 s3]]".
     * 
     * @param str the String of form "[[x1 y1 s1];[x2 y2 s2];[x3 y3 s3]]"
     */
    public AperiodicPart(String str) {
        this.segments = new SegmentList(str);
    }
	
	/**
     * Returns the segments of this AperiodicPart as a SegmentList.
     * 
     * @return the segments of this AperiodicPart as a SegmentList.
     */
    public SegmentList segments() {
        return segments;
    }

    /**
     * Scales this Segment along the x-axis. The factor must be greater than 0.
     * 
     * @param factor the factor to scale this Segment.
     */
    public void scaleX(double factor) {
        segments.scaleX(factor);
    }

    /**
     * Scales this Segment along the y-axis.
     * 
     * @param factor the factor to scale this Segment.
     */
    public void scaleY(double factor) {
        segments.scaleY(factor);
    }

    /**
     * Move this AperiodicPart by (dx,dy). If dx is positive, a Segment (0,0,0) 
     * is added at the beginning of the AperiodicPart. If dx is negative, the 
     * resulting AperiodicPart defined over x < 0 is lost.
     * 
     * @param dx the x-value by which this AperiodicPart is moved.
     * @param dy the y-value by which this AperiodicPart is moved.
     */
    public void move(double dx, double dy) {
        segments.move(dx, dy);
        if (DoubleMath.gt(dx, 0.0)) {
            if (DoubleMath.eq(this.segments.segment(0).s(), 0.0)
                    && DoubleMath.eq(this.segments.segment(0).y(), 0.0)) {
                this.segments.segment(0).setX(0.0);
            } else {
                this.segments.add(0, new Segment(0.0, 0.0, 0.0));
            }            
        } else if (DoubleMath.lt(dx, 0.0)) {
            int i = this.segments.size() - 1;
            boolean reachedNegative = false;
            Segment seg = null;
            while(i >= 0) {
                if (!reachedNegative) {
                    seg = this.segments.segment(i);
                    if (DoubleMath.eq(seg.x(), 0.0)) {
                        seg.setX(0.0);
                        reachedNegative = true;
                    } else if (DoubleMath.lt(seg.x(), 0.0)) {
                        seg.setY(seg.yAt(0.0));
                        seg.setX(0.0);
                        reachedNegative = true;
                    }                                  
                } else {
                    this.segments.remove(i);
                }
                i = i - 1;
            }           
        }
    }
    
	/**
     * Round all points that define this AperiodicPart. 
     * This includes all start points of all segments, all slopes, 
     * as well as px0, py0, pdy.
     * All related values such as pyMin and pyMax are newly calculated.
	 */
	public void round() {
	    segments.round();
    }

    /** 
     * Indicates whether some other AperiodicPart is equal to this one.
     * 
     * @param object the AperiodicPart to compare this one with.
     * @return <code>true</code> if this AperiodicPart equals object, 
     * <code>false</code> otherwise.
     */
    public boolean equals(Object object){
        if (object == null) return false;
        AperiodicPart part = (AperiodicPart) object;
        if (this == part) return true;
        return this.segments.equals(part.segments());
    }
	
    /** 
     * Creates a deep copy of this AperiodicPart.
     * 
     * @return a deep copy of this AperiodicPart.
     */
    public AperiodicPart clone() {
        return new AperiodicPart((SegmentList)this.segments().clone());
    }

    /** 
     * Returns a String representation of this AperiodicPart.
     * 
     * @return a String representation of this AperiodicPart.
     */
	public String toString() {
        return "  AperiodicPart  = " + this.segments.toString() + " \n";
	}

	/**
	 * Returns the data of this curve in a textual form for export. 
	 * The syntax is such that this String can be copied directly into Matlab
	 * to create a copy of this Curve.
     * 
	 * @return the data of this Curve in a textual form for export.
	 */
	public String toMatlabString() {
	    return this.segments.toMatlabString();
	}
		
    protected SegmentList segments;
}
