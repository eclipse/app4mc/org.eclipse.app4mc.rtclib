/* IllegalOperationException implementation.

Copyright (c) 2004-2006 Computer Engineering and Networks Laboratory (TIK), 
Swiss Federal Institute of Technology (ETH) Zurich.
All rights reserved.
Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH 
BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR 
CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS 
DOCUMENTATION, EVEN IF THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) 
ZURICH HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH SPECIFICALLY 
DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE SWISS 
FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH  HAS NO OBLIGATION TO 
PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

                                       RTC_COPYRIGHT_VERSION_1
                                       COPYRIGHTENDKEY

@ProposedRating
@AcceptedRating 

Created: 31.10.2005 by Ernesto Wandeler
*/
package org.eclipse.app4mc.rtclib;

/**
 * <code>IllegalOperationException</code> provides an exception that is thrown
 * when an illegal operation is executed. This exception is thrown for example, 
 * if an non-invertable curve is inverted.
 * 
 * @author         Ernesto Wandeler
 * @version        $Id: IllegalOperationException.java 918 2008-12-23 11:38:46Z nikolays $
 * @since          
 * @ProposedRating Green (wandeler@tik.ee.ethz.ch)
 * @AcceptedRating none
 * @JUnitRating    none (wandeler@tik.ee.thz.ch)
 */
public class IllegalOperationException extends RuntimeException {

    
    /**
     * Constructs an IllegalOperationException.
     */
    public IllegalOperationException() {
        super();
    }

    /**
     * Constructs an IllegalOperationException with a detailed message.
     * 
     * @param s the detailed message.
     */
    public IllegalOperationException(String s) {
        super(s);
    }

    private static final long serialVersionUID = 2598352218976515685L;

}

