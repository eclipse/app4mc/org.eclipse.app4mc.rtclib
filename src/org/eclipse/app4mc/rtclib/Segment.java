/* Segment implementation.

Copyright (c) 2004-2006 Computer Engineering and Networks Laboratory (TIK), 
Swiss Federal Institute of Technology (ETH) Zurich.
All rights reserved.
Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH 
BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR 
CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS 
DOCUMENTATION, EVEN IF THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) 
ZURICH HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH SPECIFICALLY 
DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE SWISS 
FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH  HAS NO OBLIGATION TO 
PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

                                       RTC_COPYRIGHT_VERSION_1
                                       COPYRIGHTENDKEY

@ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
@AcceptedRating 
@JUnitTesting	Yellow (wandeler@tik.ee.ethz.ch)

Created: 28.10.2005 by Ernesto Wandeler
*/
package org.eclipse.app4mc.rtclib;

import java.io.Serializable;

import org.eclipse.app4mc.rtclib.util.DoubleMath;
import org.eclipse.app4mc.rtclib.util.MathUtil;
import org.eclipse.app4mc.rtclib.util.Messages;

/**
 * <code>Segment</code> represents a single curve segment.
 * 
 * @author         Ernesto Wandeler
 * @version        $Id: Segment.java 968 2015-09-11 11:37:36Z thiele $
 * @since          
 * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
 * @AcceptedRating none
 * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
 */
public class Segment implements Cloneable, Serializable {
	
    // is set in the build.xml (replace while copying files)
	// private static final long serialVersionUID = Integer.parseInt("@serialVersionUID@");	
	private static final long serialVersionUID = 1598352218976515684L;
	
	///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

	/**
	 * Constructs a Segment with a given start point and slope.
     * 
	 * @param x the x-coordinate of the start point.
	 * @param y the y-coordinate of the start point.
	 * @param s the slope of the Segment.
	 */
	public Segment(double x, double y, double s) {
		this.x = x;
		this.y = y;
		this.s = s;
	}
    
    /**
     * Constructs a Segment from a String of the form "[x y s]".
     * 
     * @param str the String of form "[x y s]"
     */
    public Segment(String str) {
        String[] elem;
        str = str.trim();
        str = str.substring(str.indexOf("[") + 1, str.lastIndexOf("]")).trim();
        elem = str.split(" ");
        this.x = Double.valueOf(elem[0]).doubleValue();
        this.y = Double.valueOf(elem[1]).doubleValue(); 
        this.s = Double.valueOf(elem[2]).doubleValue();   
    }
	
	/**
	 * Returns the x-coordinate of the start point of this Segment.
     * 
	 * @return the x-coordinate of the start point of this Segment.
	 */
	public double x() {
		return x;
	}
	
	/**
	 * Returns the y-coordinate of the start point of this Segment.
     * 
	 * @return the y-coordinate of the start point of this Segment.
	 */
	public double y() {
		return y;
	}
	
	/**
	 * Returns the slope of this Segment.
     * 
	 * @return the slope of this Segment.
	 */
	public double s() {
		return s;
	}
	
	/**
	 * Returns the y-value of this Segment at x0.
     * 
	 * @param x0 the x-value at which the returned y-value is computed. 
	 * @return the y-value of this Segment at x0.
	 */
	public double yAt(double x0) {
        if (Double.isInfinite(x0)) {
            if (DoubleMath.eq(s, 0.0)) {
                return y;
            } else if (DoubleMath.lt(s, 0.0)) {
                return Double.NEGATIVE_INFINITY;
            } else {
                return Double.POSITIVE_INFINITY;
            }
        } else if (DoubleMath.eq(x0, x)) {
            return y;
        }
		return (y + (s * (x0 - x)));
	}
    
    /**
     * Returns the x-value of this Segment at y0.
     * 
     * @param y0 the y-value at which the returned x-value is computed. 
     * @return the x-value of this Segment at y0.
     */
    public double xAt(double y0) {
        if (Double.isInfinite(y0)) {
            if (Double.isInfinite(s)) {
                return x;
            } else {
                return Double.POSITIVE_INFINITY;
            }
        } else if (DoubleMath.eq(y0, y)) {
            return x;
        } else if (DoubleMath.eq(s, 0.0)) {
            return Double.NaN;
        }
        return (x + ((y0 - y) / s));
    }
	
	/**
	 * Set the x-coordinate of this Segment to x.
     * 
	 * @param x the new value of x.
	 */
	public void setX(double x) {
	    this.x = x;
	}
	
	/**
	 * Set the y-coordinate of this Segment to y.
     * 
	 * @param y the new value of y.
	 */
	public void setY(double y) {
	    this.y = y;
	}
	
	/**
	 * Set the slope of this Segment to s.
     * 
	 * @param s the new value of slope.
	 */
	public void setS(double s) {
        
	    this.s = s;
	}

    /**
     * Scales this Segment along the x-axis. The factor must be greater than 0.
     * 
     * @param factor the factor to scale this Segment.
     */
    public void scaleX(double factor) {
        if (DoubleMath.leq(factor, 0)) {
            throw new IllegalArgumentException(
                    Messages.SEGMENT_SCALEX_FACTOR_LEQ_0);
        }           
        this.x *= factor;
        this.s /= factor;
    }
    
    /**
     * Scales this Segment along the y-axis.
     * 
     * @param factor the factor to scale this Segment.
     */
    public void scaleY(double factor) {
        this.y *= factor;
        this.s *= factor;
    }
    
	/**
	 * Moves the start point of this Segment by (dx, dy).
     * 
	 * @param dx the value by which this Segment is moved on the x-axis.
	 * @param dy the value by which this Segment is moved on the y-axis.
	 */
	public void move(double dx, double dy) {
		this.x += dx;
		this.y += dy;
	}
	
    /**
     * Returns true if this Segment equals the passed Segment after 
     * it is moved by (dx, dy).
     * 
     * @param dx the x-value by which this Segment is moved for the 
     * comparison.
     * @param dy the y-value by which this Segment is moved for the 
     * comparison.
     * @param seg the Segment to compare,
     * @return boolean result of the comparison.
     */
    public boolean moveEquals (double dx, double dy, Segment seg) {
    	if (seg == null) return false;
    	if (this.s == Double.POSITIVE_INFINITY || this.y == Double.POSITIVE_INFINITY){
    		if (seg.s == Double.POSITIVE_INFINITY || seg.y == Double.POSITIVE_INFINITY){
    			return true;
    		}
    	}
        if (DoubleMath.neq(this.s(), seg.s())) return false;
        if (DoubleMath.neq(this.x() + dx, seg.x())) return false;
        if (DoubleMath.neq(this.y() + dy, seg.y())) return false;
        return true;	
    }

    /**
     * Round x, y and slope to the given precision. 
     */
    public void round() {
	    x = MathUtil.round(x);
	    y = MathUtil.round(y);
	    s = MathUtil.round(s);
	}
	
    /** 
     * Indicates whether some other Segment is equal to this one.
     * 
     * @param obj the Segment to compare this one with.
     * @return <code>true</code> if this Segment equals object, 
     * <code>false</code> otherwise.
     */
	public boolean equals(Object obj) {
		if (obj == null) return false;
        Segment seg = (Segment) obj;
        if (DoubleMath.neq(seg.s(), this.s())) return false;
        if (DoubleMath.neq(seg.x(), this.x())) return false;
        if (DoubleMath.neq(seg.y(), this.y())) return false;
        return true;		
	}
	
    /** 
     * Creates a copy of this Segment.
     * 
     * @return a copy of this Segment.
     */
	public Segment clone() {
		return new Segment(x, y, s);
	}
	
    /** 
     * Returns a String representation of this Segment.
     * 
     * @return a String representation of this Segment.
     */
	public String toString() {
		return "(" + x + "," + y + "," + s + ")"; 
	}
	
    /**
     * Returns the data of this Segment in a textual form for export. 
     * 
     * @return the data of this Segment in a textual form for export.
     */
    public String toExportString() {
		return "[" + x + " " + y + " " + s + "]"; 	    
	}


	///////////////////////////////////////////////////////////////////
    ////                         private variables                 ////
	
	protected double x;
    protected double y;
    protected double s;
}
