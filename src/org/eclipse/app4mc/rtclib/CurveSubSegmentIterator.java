/* CurveSubSegmentIterator implementation.

Copyright (c) 2004-2006 Computer Engineering and Networks Laboratory (TIK), 
Swiss Federal Institute of Technology (ETH) Zurich.
All rights reserved.
Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH 
BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR 
CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS 
DOCUMENTATION, EVEN IF THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) 
ZURICH HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH SPECIFICALLY 
DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE SWISS 
FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH  HAS NO OBLIGATION TO 
PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

                                       RTC_COPYRIGHT_VERSION_1
                                       COPYRIGHTENDKEY

@ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
@AcceptedRating 
@JUnitTesting   Yellow (wandeler@tik.ee.ethz.ch)

Created: 02.11.2005 by Ernesto Wandeler
*/
package org.eclipse.app4mc.rtclib;

import org.eclipse.app4mc.rtclib.util.DoubleMath;

/**
 * <code>CurveSubSegmentIterator</code> provides an iterator that iterates over
 * the single curve sub-segments of two curves.
 * 
 * @author         Ernesto Wandeler
 * @version        $Id: CurveSubSegmentIterator.java 377 2007-03-20 20:10:09Z nikolays $
 * @since          
 * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
 * @AcceptedRating none
 * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
 */
public class CurveSubSegmentIterator {

    /**
     * Creates an CurveSubSegmentIterator that iterates over all Subsegments of 
     * the Curves a and b, from x=0 up to and including x=xMax.<br><br>
     * Subsegments are defined as follows:<br> 
     * X_A is the ordered list of the x-values of the start-points of all 
     * CurveSegments of Curve a from x=0 up to x=xMax.<br>
     * X_B is the ordered list of the x-values of the start-points of all 
     * CurveSegments of Curve b from x=0 up to x=xMax.<br>
     * X_AB is the ordered union of X_A and X_B without any duplicates.<br>
     * X_AB contains the x-values of the start and end points of all Subsegments
     * of the Curve a and b. A Subsegment therefore starts at X_AB[i] and ends 
     * at X_AB[i+1].<br><br>
     * Subsegments are of interest, because both Curves a and b are continuous
     * and do not change their slope during a subsegment.
     * 
     * @param a the first Curve.
     * @param b the second Curve.
     * @param xMax the upper value up to which this CurveSubSegmentIterator 
     * should iterate.
     */
    public CurveSubSegmentIterator(Curve a, Curve b, double xMax) {
        if (a != null && b != null) {
            this.a = a;
            this.b = b;
            this.iterA = a.segmentIterator(xMax);
            this.iterB = b.segmentIterator(xMax);
            this.init = false;
            this.pointer = -1;
        }
    }
    
    /**
     * Creates an CurveSubSegmentIterator that iterates over all Subsegments of 
     * the Curves a and b.<br><br>
     * Subsegments are defined as follows:<br> 
     * X_A is the ordered list of the x-values of the start-points of all 
     * Segments of Curve a.<br>
     * X_B is the ordered list of the x-values of the start-points of all 
     * Segments of Curve b.<br>
     * X_AB is the ordered union of X_A and X_B without any duplicates.<br>
     * X_AB contains the x-values of the start and end points of all Subsegments
     * of the Curve a and b. A Subsegment therefore starts at X_AB[i] and ends 
     * at X_AB[i+1].<br><br>
     * Subsegments are of interest, because both Curves a and b are continuous
     * and do not change their slope during a subsegment.
     * 
     * @param a the first Curve.
     * @param b the second Curve.
     */
    public CurveSubSegmentIterator(Curve a, Curve b) {
        this(a, b, Double.POSITIVE_INFINITY);
    }
    
	/**
	 * Advances this CurveSubSegmentIterator to the next Subsegment and returns 
     * true, if this CurveSubSegmentIterator did not reach its end yet. 
     * 
	 * @return true, if this CurveSubSegmentIterator did not reach its end yet.
	 */
	public boolean next() {
        if (a == null || b == null) {
            return false;
        }
        if (!init) {
            iterA.next();
            iterB.next();
            init = true;
            aIsNew = true;
            bIsNew = true;
            xEnd = 0.0;
        } else {
//            if (DoubleMath.eq(xEnd, xMax)) {
//                return false;
//            }
        	
            if (DoubleMath.eq(iterA.xEnd(), xEnd)) {
                aIsNew = iterA.next();
            } else {
                aIsNew = false;
            }
            
            if (DoubleMath.eq(iterB.xEnd(), xEnd)) {
                bIsNew = iterB.next();
            } else {
                bIsNew = false;
            }
            
            if (!aIsNew && !bIsNew) {
                return false;
            }
        }
        
        xStart = xEnd;
        
        xEnd = Math.min(iterA.xEnd(), iterB.xEnd());
        
        segmentA = iterA.segment();
        segmentB = iterB.segment();
        
        yStartA = segmentA.yAt(xStart);
        yEndA = segmentA.yAt(xEnd);
        
        yStartB = segmentB.yAt(xStart);
        yEndB = segmentB.yAt(xEnd);
        
        pointer = pointer + 1;
        
		return true;
	}
    
    /**
     * Resets this CurveSubSegmentIterator.
     */
    public void reset() {
        if (a != null && b != null) {
            iterA.reset();
            iterB.reset();
            this.init = false;
            this.pointer = -1;
        }        
    }
	
	/**
	 * Returns the x-value of the start of the current Subsegment.
     * 
	 * @return the x-value of the start of the current Subsegment.
	 */
	public double xStart() {
        if (!init) throw new IllegalStateException();
	    return xStart;
	}
	
	/**
	 * Returns the x-value of the end of the current Subsegment.
     * 
	 * @return the x-value of the end of the current Subsegment.
	 */
	public double xEnd() {
        if (!init) throw new IllegalStateException();
	    return xEnd;
	}

	/**
	 * Returns the current Segment of Curve a.
     * 
	 * @return the current Segment of Curve a.
	 */
	public Segment segmentA() {
        if (!init) throw new IllegalStateException();
	    return segmentA;
	}
	
	/**
	 * Returns the current Segment of Curve b.
     * 
	 * @return the current Segment of Curve b.
	 */
	public Segment segmentB() {
        if (!init) throw new IllegalStateException();
	    return segmentB;
	}
    
	/**
	 * Returns the current subsegment of Curve a.
     * 
	 * @return the current subsegment of Curve a.
	 */
	public Segment subSegmentA() {
        if (!init) throw new IllegalStateException();
	    return new Segment(xStart, yStartA, segmentA.s());
	}
	
	/**
	 * Returns the current subsegment of Curve b.
     * 
	 * @return the current subsegment of Curve b.
	 */
	public Segment subSegmentB() {
        if (!init) throw new IllegalStateException();
	    return new Segment(xStart, yStartB, segmentB.s());
	}
	
	/**
	 * Returns the y-value of Curve a at the start of the current Subsegment.
     * 
	 * @return the y-value of Curve a at the start of the current Subsegment.
	 */
	public double yStartA() {
        if (!init) throw new IllegalStateException();
	    return yStartA;
	}
	
	/**
	 * Returns the y-value of Curve a at the end of the current Subsegment.
     * 
	 * @return the y-value of Curve a at the end of the current Subsegment.
	 */
	public double yEndA() {
        if (!init) throw new IllegalStateException();
	    return yEndA;
	}
	
	/**
	 * Returns the slope of the current Segment of Curve a.
     * 
	 * @return the slope of the current Segment of Curve a.
	 */
	public double sA() {
        if (!init) throw new IllegalStateException();
	    return segmentA.s();
	}
	
	/**
	 * Returns the y-value of Curve b at the start of the current Subsegment.
     * 
	 * @return the y-value of Curve b at the start of the current Subsegment.
	 */
	public double yStartB() {
        if (!init) throw new IllegalStateException();
	    return yStartB;
	}
	
	/**
	 * Returns the y-value of Curve b at the end of the current Subsegment.
     * 
	 * @return the y-value of Curve b at the end of the current Subsegment.
	 */
	public double yEndB() {
        if (!init) throw new IllegalStateException();
	    return yEndB;
	}
	
	/**
	 * Returns the slope of the current Segment of Curve b.
     * 
	 * @return the slope of the current Segment of Curve b.
	 */
	public double sB() {
        if (!init) throw new IllegalStateException();
	    return segmentB.s();
	}
	
	/**
	 * Returns true, if in the current Subsegment, a new Segment of Curve
	 * a was used. I.e. if the x-value of the start of the current Subsegment
	 * equals the x-value of the start of the current Segment of Curve a. 
     * 
	 * @return true, if in the current Subsegment, a new Segment of Curve
	 * a was used.
	 */
	public boolean aIsNew() {
        if (!init) throw new IllegalStateException();
	    return aIsNew;
	}
	
	/**
	 * Returns true, if in the current Subsegment, a new Segment of Curve
	 * b was used. I.e. if the x-value of the start of the current Subsegment
	 * equals the x-value of the start of the current Segment of Curve b. 
     * 
	 * @return true, if in the current Subsegment, a new Segment of Curve
	 * b was used.
	 */
	public boolean bIsNew() {
        if (!init) throw new IllegalStateException();
	    return bIsNew;
	}
	
    /**
     * Returns the index of the current SubSegment.
     * 
     * @return the index of the current SubSegment.
     */
    public int index() {
        if (!init) throw new IllegalStateException();
        return pointer;
    }
    
    private Curve a, b;
	private CurveSegmentIterator iterA, iterB;
	private int pointer;
    private double xStart, xEnd;
	private Segment segmentA, segmentB;
	private double yStartA, yEndA; 
	private double yStartB, yEndB;
	private boolean aIsNew, bIsNew, init;
 }