/* CurveSegmentIterator implementation.

Copyright (c) 2004-2006 Computer Engineering and Networks Laboratory (TIK), 
Swiss Federal Institute of Technology (ETH) Zurich.
All rights reserved.
Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH 
BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR 
CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS 
DOCUMENTATION, EVEN IF THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) 
ZURICH HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH SPECIFICALLY 
DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE SWISS 
FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH  HAS NO OBLIGATION TO 
PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

                                       RTC_COPYRIGHT_VERSION_1
                                       COPYRIGHTENDKEY

@ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
@AcceptedRating 
@JUnitTesting   Yellow (wandeler@tik.ee.ethz.ch)

Created: 01.11.2005 by Ernesto Wandeler
*/

package org.eclipse.app4mc.rtclib;

import org.eclipse.app4mc.rtclib.util.DoubleMath;


/**
 * <code>CurveSegmentIterator</code> provides an iterator that iterates over
 * the single curve segments of a curve.
 * 
 * @author         Ernesto Wandeler
 * @version        $Id: CurveSegmentIterator.java 918 2008-12-23 11:38:46Z nikolays $
 * @since          
 * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
 * @AcceptedRating none
 * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
 */
public class CurveSegmentIterator {
    
    /**
     * Creates a CurveSegmentIterator that iterates over all Segments in
     * the passed Curve, up to x <= xMax. 
     * 
     * @param c the Curve to iterate over.
     * @param xMax the maximum value up to which to iterate.
     */
    public CurveSegmentIterator(Curve c, double xMax) {
        if (c != null) {
            this.c = c;
            if (Double.isNaN(xMax) || DoubleMath.lt(xMax, 0.0)) {
                this.xMax = Double.POSITIVE_INFINITY;
            } else {
                this.xMax = xMax;
            }
            this.isLimited = (this.xMax!=Double.POSITIVE_INFINITY);
            if (c.hasAperiodicPart()) {
                if (c.hasPeriodicPart()) {
                    this.aper = c.aperiodicSegments()
                            .segmentListIterator(Math.min(c.px0(), this.xMax));
                } else {
                    this.aper = c.aperiodicSegments()
                            .segmentListIterator(this.xMax);
                }
                this.isInAperMode = true;
            } else {
                this.isInAperMode = false;
            }
            if (c.hasPeriodicPart()) {
                this.per = c.periodicSegments().segmentListIterator(c.pdx());
                this.perCounter = 0;
            }
        }
        this.init = false;
        this.lastElement = false;
        this.pointer = -1;
    }
    
    /**
     * Creates a CurveSegmentIterator that iterates over all Segments in
     * the passed Curve. 
     * 
     * @param c the Curve to iterate over.
     */
    public CurveSegmentIterator(Curve c) {
        this(c, Double.NaN);
    }
    
	/**
	 * Advances this Iterator to the next Segment in the Curve and returns 
     * true if the end is not reached yet.
     *
	 * @return true, if the end of the SegmentList is not reached yet.
	 */
	public boolean next() {
        if (c == null) {
            return false;
        }
        init = true;
        if (lastElement) {
            return false;
        }
        if (isInAperMode) {
            if (aper.next()) {
                seg = (Segment)aper.segment().clone();
                xStart = aper.xStart();
                yStart = aper.yStart();
                xEnd = aper.xEnd();
                yEnd = aper.yEnd();
            } else {
                if (c.hasPeriodicPart()) {
                    if (DoubleMath.lt(xMax, c.px0())) {
                        return false;
                    } else {
                        isInAperMode = false;
                    }
                } else {
                    return false;
                }
            }
        } 
        if (!isInAperMode) {
            if (!per.next()) {
                per.reset();
                per.next();
                perCounter++;
            }
            dx = c.px0() + (perCounter * c.pdx());
            dy = c.py0() + (perCounter * c.pdy());
            seg = (Segment)per.segment().clone();
            seg.move(dx, dy);
            xStart = seg.x();
            yStart = seg.y();
            //xEnd = Math.min(per.xEnd() + dx, xMax);
            
//            xEnd = per.xEnd() + dx;
            if (isLimited && DoubleMath.gt(per.xEnd() + dx, xMax)) {
                xEnd = xMax;
                yEnd = seg.yAt(xEnd);
                lastElement = true;
            } else {
                xEnd = per.xEnd() + dx;
                yEnd = per.yEnd() + dy;
            }
            
//            if (isLimited && DoubleMath.eq(xEnd, xMax)) {
//                
//            } else {
//                
//            }
        } 
        pointer = pointer + 1;
        return true;
	}
	
//	/**
//     * Moves this Iterator to the previous Segment in the SegmentList and 
//     * returns true if the start of the SegmentList is not reached yet.
//     * 
//	 * @return true, if the Iterator did not reach its start yet.
//	 */
//	public boolean prev() {
//      FIXME: to be implemented
//	}
	
	/**
	 * Resets this CurveSegmentIterator.
	 */
	public void reset() {
        if (c != null) {
            if (c.hasAperiodicPart()) {
                aper.reset();
                isInAperMode = true;
            } else {
                isInAperMode = false;
            }
            if (c.hasPeriodicPart()) {
                per.reset();
                perCounter = 0;
            }
        }
        init = false;
        lastElement = false;
        pointer = -1;
	}
	
	/**
	 * Returns the current Segment.
     * 
	 * @return the current Segment.
	 */
	public Segment segment() {
        if (!init) throw new IllegalStateException();
	    return seg;
	}
    
	/**
	 * Returns the x-value of the start of the current Segment.
     * 
	 * @return the x-value of the start of the current Segment.
	 */
	public double xStart() {
        if (!init) throw new IllegalStateException();
	    return xStart;
	}
	
	/**
	 * Returns the x-value of the end of the current Segment.
     * 
	 * @return the x-value of the end of the current Segment.
	 */
	public double xEnd() {
        if (!init) throw new IllegalStateException();
	    return xEnd;
	}
	
	/**
	 * Returns the y-value of the start of the current Segment.
     * 
	 * @return the y-value of the start of the current Segment.
	 */
	public double yStart() {
        if (!init) throw new IllegalStateException();
	    return yStart;
	}
	
	/**
	 * Returns the y-value of the end of the current Segment.
     * 
	 * @return the y-value of the end of the current Segment.
	 */
	public double yEnd() {
        if (!init) throw new IllegalStateException();
	    return yEnd;
	}
	
	/**
	 * Returns the slope of the current Segment.
     * 
	 * @return the slope of the current Segment.
	 */
	public double s() {
        if (!init) throw new IllegalStateException();
	    return seg.s;
	}

    /**
     * Returns the index of the current Segment.
     * 
     * @return the index of the current Segment.
     */
    public int index() {
        if (!init) throw new IllegalStateException();
        return pointer;
    }
    
    private Curve c = null;
    private Segment seg;
    private SegmentListIterator aper, per;
    private boolean isInAperMode, init, isLimited, lastElement;
    private int perCounter, pointer;
    private double xStart, xEnd, yStart, yEnd, dx, dy, xMax;
 }