/* LimitedSegment implementation.

Copyright (c) 2004-2006 Computer Engineering and Networks Laboratory (TIK), 
Swiss Federal Institute of Technology (ETH) Zurich.
All rights reserved.
Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH 
BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR 
CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS 
DOCUMENTATION, EVEN IF THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) 
ZURICH HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH SPECIFICALLY 
DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE SWISS 
FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH  HAS NO OBLIGATION TO 
PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

                                       RTC_COPYRIGHT_VERSION_1
                                       COPYRIGHTENDKEY

@ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
@AcceptedRating 
@JUnitTesting   Yellow (wandeler@tik.ee.ethz.ch)

Created: 28.10.2005 by Ernesto Wandeler
*/
package org.eclipse.app4mc.rtclib;

import org.eclipse.app4mc.rtclib.util.DoubleMath;
import org.eclipse.app4mc.rtclib.util.MathUtil;
import org.eclipse.app4mc.rtclib.util.Messages;

/**
 * <code>CurveSegment</code> represents a limited curve segment. That is a curve
 * segment with a start- and an end-point.
 * 
 * @author         Ernesto Wandeler
 * @version        $Id: LimitedSegment.java 968 2015-09-11 11:37:36Z thiele $
 * @since          
 * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
 * @AcceptedRating none
 * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
 */
public class LimitedSegment extends Segment implements Comparable<Segment> {

    // is set in the build.xml (replace while copying files)
	// private static final long serialVersionUID = Integer.parseInt("@serialVersionUID@");	
	private static final long serialVersionUID = 1598352218976515684L;
	
	
	/**
	 * Creates a LimitedSegment with a given start point, slope and xEnd.
     * 
	 * @param x the x-coordinate of the start point.
	 * @param y the y-coordinate of the start point.
	 * @param s the slope of the curve segment.
     * @param xEnd the x-coordinate of the end point.
	 */
    public LimitedSegment(double x, double y, double s, double xEnd) {
        super(x, y, s);
        this.xEnd = xEnd;
        this.yEnd = this.y + ((this.xEnd - this.x) * this.s);
        if (DoubleMath.lt(this.xEnd, this.x)) {
          	throw new IllegalArgumentException(
           	        Messages.LIMITEDSEGMENT_ENDX_LT_X);
        }
    }
    
	/**
	 * Returns the x-coordinate of the end point of this curve segment.
     * 
	 * @return the x-coordinate of the end point of this curve segment.
	 */
    public double xEnd() {
        return xEnd;
    }
    
	/**
	 * Returns the y-coordinate of the end point of this curve segment.
     * 
	 * @return the y-coordinate of the end point of this curve segment.
	 */
    public double yEnd() {
        return yEnd;
    }
    
    public void setXEnd(double endX) {
        if (DoubleMath.lt(endX, this.x)) 
        	throw new IllegalArgumentException(
        	        Messages.LIMITEDSEGMENT_ENDX_LT_X);
        this.xEnd = endX;
        this.yEnd = this.y + ((this.xEnd - this.x) * this.s);
    }
    
	/**
	 * Returns the y-value of this LimitedSegment at x0. If x0 does not
	 * lie inside [x,xEnd] of this LimitedSegment, a Double.NaN is returned.
     * 
	 * @param x0 the x-value at which the returned y-value is computed. 
	 * @return the y-value of this curve segment at x0.
	 */
    public double yAt(double x0) {
        if (DoubleMath.leq(this.x, x0) && DoubleMath.geq(this.xEnd, x0)) {
            return super.yAt(x0);
        } else {
            return Double.NaN;
        }
    }
    
	/**
	 * Scales this LimitedSegment along the x-axis.
     * 
	 * @param factor the factor to scale this LimitedSegment.
	 */
    public void scaleX(double factor) {
        if (DoubleMath.leq(factor, 0)) {
            throw new IllegalArgumentException(
                    Messages.SEGMENT_SCALEX_FACTOR_LEQ_0);
        }           
        super.scaleX(factor);
        this.xEnd *= factor;
    }
    
    /**
     * Scales this LimitedSegment along the y-axis.
     * 
     * @param factor the factor to scale this LimitedSegment.
     */
    public void scaleY(double factor) {
        super.scaleY(factor);
        this.yEnd *= factor;
    }    
    
    /**
     * Moves this LimitedSegment by (dx, dy).
     * 
     * @param dx the value by which this LimitedSegment is moved on the x-axis.
     * @param dy the value by which this LimitedSegment is moved on the y-axis.
     */
    public void move(double dx, double dy) {
        super.move(dx, dy);
        this.xEnd += dx;
        this.yEnd += dy;
    }
    
    /**
     * Round x, y, xEnd, yEnd and s to the given precision. 
     */
    public void round() {
        super.round();
        xEnd = MathUtil.round(xEnd);
        yEnd = MathUtil.round(yEnd);
    }

    /** 
     * Indicates whether some other LimitedSegment is equal to this one.
     * 
     * @param obj the LimitedSegment to compare this one with.
     * @return <code>true</code> if this LimitedSegment equals object, 
     * <code>false</code> otherwise.
     */
	public boolean equals(Object obj) {
		if (obj == null) return false;
        LimitedSegment seg = (LimitedSegment) obj;
        if (DoubleMath.neq(seg.s(), this.s())) return false;
        if (DoubleMath.neq(seg.x(), this.x())) return false;
        if (DoubleMath.neq(seg.y(), this.y())) return false;
        if (DoubleMath.neq(seg.xEnd(), this.xEnd())) return false;
        if (DoubleMath.neq(seg.yEnd(), this.yEnd())) return false;
        return true;		
	}
	
	/**
	 * Returns true if this LimitedSegment equals the passed LimitedSegment 
     * after it is shifted by (dx, dy), and false otherwise.
	 * @param dx the x-value by which this LimitedSegment is shifted for the 
	 * comparison.
	 * @param dy the y-value by which this LimitedSegment is shifted for the 
	 * comparison.
	 * @param seg the LimitedSegment to compare,
	 * @return boolean result of the comparison.
	 */
	public boolean moveEquals (double dx, double dy, LimitedSegment seg) {
        if (seg == null) return false;
        if (DoubleMath.neq(this.s(), seg.s())) return false;
        if (DoubleMath.neq(this.x() + dx, seg.x())) return false;
        if (DoubleMath.neq(this.y() + dy, seg.y())) return false;
        if (DoubleMath.neq(this.xEnd() + dx, seg.xEnd())) return false;
        if (DoubleMath.neq(this.yEnd() + dy, seg.yEnd())) return false;
        return true;	
	}

    /** 
     * Creates a copy of this LimitedSegment.
     * 
     * @return a copy of this LimitedSegment.
     */
	public LimitedSegment clone() {
		return new LimitedSegment(x, y, s, xEnd);
	}
	
    /** 
     * Returns a String representation of this LimitedSegment.
     * 
     * @return a String representation of this LimitedSegment.
     */
	public String toString() {
		return "(" + x + "," + y + "," + s + "," + xEnd + "," + yEnd + ")"; 
	}

    /**
     * Returns the data of this LimitedSegment in a textual form for export. 
     * 
     * @return the data of this LimitedSegment in a textual form for export.
     */    
	public String toExportString() {
		return "[" + x + " " + y + " " + s + " " + xEnd + " " + yEnd + "]"; 	    
	}
	
	/**
     * Returns a comparison of this LimitedSegment to another LimitedSegment.
     * A LimitedSegment is considered to be smaller than another LimitedSegment, 
     * if the x-coordinate of
     * its start point is smaller than the x-coordinate of start point of the 
     * other LimitedSegment, or if the x-coordinates of both start points are 
     * equal and the x-coordinate of the end point is smaller than the 
     * x-coordinate of the end point of the other LimitedSegment, or if the
     * x-coordinates of the start and end points are equal and the slope is 
     * smaller than the slope of the other segment. Otherwise the two 
     * LimitedSegments are considered to be equal.
     * 
	 * @param obj the LimitedSegment to compare with this LimitedSegment.
	 * @return the comparison result
	 */
	public int compareTo(Segment obj) {
		if (obj == null) throw new IllegalArgumentException();
	    LimitedSegment seg = (LimitedSegment) obj;
	    if (DoubleMath.lt(this.x, seg.x())) return -1;
	    if (DoubleMath.gt(this.x, seg.x())) return 1;
	    if (DoubleMath.lt(this.xEnd, seg.xEnd())) return -1;
	    if (DoubleMath.gt(this.xEnd, seg.xEnd())) return 1;
	    if (DoubleMath.lt(this.s, seg.s())) return -1;
	    if (DoubleMath.gt(this.s, seg.s())) return 1;
	    return 0;
	}
    
    protected double xEnd;
    protected double yEnd;
}


