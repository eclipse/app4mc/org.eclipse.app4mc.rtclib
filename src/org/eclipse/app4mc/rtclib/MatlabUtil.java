/* MatlabUtil utilities for the use of Matlab as frontend.

Copyright (c) 2004-2006 Computer Engineering and Networks Laboratory (TIK), 
Swiss Federal Institute of Technology (ETH) Zurich.
All rights reserved.
Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH 
BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR 
CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS 
DOCUMENTATION, EVEN IF THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) 
ZURICH HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH SPECIFICALLY 
DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE SWISS 
FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH  HAS NO OBLIGATION TO 
PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

                                       RTC_COPYRIGHT_VERSION_1
                                       COPYRIGHTENDKEY

@ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
@AcceptedRating 
@JUnit			Yellow (wandeler@tik.ee.ethz.ch) 

Created: 17.12.2004 by Ernesto Wandeler
Updated: 01.11.2005 by Ernesto Wandeler
*/
package org.eclipse.app4mc.rtclib;

import org.eclipse.app4mc.rtclib.util.Messages;


/**
 * <code>MatlabUtil</code> provides utilities for the use of Matlab as frontend.
 * 
 * @author         Ernesto Wandeler
 * @version        $Id: MatlabUtil.java 923 2009-01-30 13:00:42Z nikolays $
 * @since          
 * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
 * @AcceptedRating none
 * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
 */
public class MatlabUtil {
    
    private static int INFINITE_SLOPE_LENGTH = 100;
	
	/**
	 * Returns a double[] containing a sampled version of curve. Curve is 
	 * sampled within 0 <= x <= xMax, with a sample period of samplePeriod.
     * To plot the sample curve in Matlab, the following command can be used:
     * plot([0:samplePeriod:xMax],MatlabUtil.sampleCurve(curve,xMax,samplePeriod),'b.')
     * 
	 * @param curve the Curve to sample.
	 * @param xMax the upper x-value of the sample range.
	 * @param samplePeriod the sample period.
	 * @return a sampled version of curve.
	 */
	public static double[] sampleCurve(Curve curve, double xMax, double samplePeriod) {
		int i, j, startIndex, nextStartIndex, numberOfSegments, numberOfSamples;
		double startX, nextStartX, dy;
		double[] sampleCurve;
		Segment segment, nextSegment;
		SegmentList unfolded;

		numberOfSamples = (int) Math.floor((xMax / samplePeriod)) + 1;
		sampleCurve = new double[numberOfSamples];
		unfolded = curve.segmentsLEQ(xMax);
		numberOfSegments = unfolded.size();
		segment = unfolded.segment(0);
		startX = segment.x();
		startIndex = (int) Math.ceil((startX / samplePeriod));
		for (i = 1; i < numberOfSegments; i++) {
			nextSegment = unfolded.segment(i);
			nextStartX = nextSegment.x();
			nextStartIndex = (int) Math.ceil((nextStartX / samplePeriod));
			sampleCurve[startIndex] = segment.yAt(samplePeriod * startIndex);
			if (segment.s() == 0) {
				for (j = startIndex; j < nextStartIndex - 1; j++) {
					sampleCurve[j + 1] = sampleCurve[j];
				}
			} else {
				dy = samplePeriod * segment.s();
				for (j = startIndex; j < nextStartIndex - 1; j++) {
					sampleCurve[j + 1] = sampleCurve[j] + dy;
				}				
			}
			segment = nextSegment;
			startX = nextStartX;
			startIndex = nextStartIndex;
		}
		if (startIndex < numberOfSamples) {
			sampleCurve[startIndex] = segment.yAt(samplePeriod * startIndex);
			if (segment.s() == 0) {
				for (j = startIndex; j < numberOfSamples - 1; j++) {
					sampleCurve[j + 1] = sampleCurve[j];
				}
			} else {
				dy = samplePeriod * segment.s();
				for (j = startIndex; j < numberOfSamples - 1; j++) {
					sampleCurve[j + 1] = sampleCurve[j] + dy;
				}				
			}
		}
		/** NS: Returned y-values should correspond directly
		 * to the segments of the curve as represented inside of
		 * the RTC Toolbox. There is no interpretation of the curve
		 * as left- or right- continuous. For this the appropriate
		 * methods should be used: yXMinusEpsilon, yXPlusEpsilon.
		 * Therefore sampleCurve[0] should contain the actual
		 * value of the curve at x = 0.
		 */
        // sampleCurve[0] = 0.0;
		return sampleCurve;
	}
	
	/**
	 * Returns a double[][] that is used by Matlab to plot curve from x=0
	 * up to x=xMax.
     * 
	 * @param curve the Curve to plot.
	 * @param xMax the upper x-value of the plot range. 
	 * @return the data of curve that is used by Matlab to plot curve from x=0
	 * up to x=xMax.
	 */
	public static double[][] printCurve(Curve curve, double xMax) {
		double[][] printCurve;
		SegmentList unfolded = curve.segmentsLT(xMax);
		printCurve = new double[(2 * unfolded.size()) + 1][2];
		SegmentListIterator iter = unfolded.segmentListIterator(xMax);
        printCurve[0][0] = 0.0;
        printCurve[0][1] = 0.0;
        while (iter.next()) {
            printCurve[(iter.index() * 2) + 1][0] = iter.xStart();
            printCurve[(iter.index() * 2) + 1][1] = iter.yStart();
            if (!Double.isInfinite(iter.yEnd())) {
                printCurve[(iter.index() * 2) + 2][0] = iter.xEnd();
                printCurve[(iter.index() * 2) + 2][1] = iter.yEnd();
            } else {
                printCurve[(iter.index() * 2) + 2][0] = iter.xStart();
                printCurve[(iter.index() * 2) + 2][1] = iter.yStart() + INFINITE_SLOPE_LENGTH;
            }
        }
        return printCurve;
	}
	
	/**
	 * Returns an ArrayList of CurveSegments that are described by data.
     * 
	 * @param data the description of the CurveSegments.
	 * @return an ArrayList of CurveSegments that are described by data.
	 */
	public static SegmentList double2SegmentList(double[][] data) {
        SegmentList list = new SegmentList(data.length);
		for (int i = 0; i < data.length; i++) {
			list.add(new Segment(data[i][0], data[i][1], data[i][2]));
		}
		return list;
	}

	/**
     * Creates a Curve from a double[][] that was previously exported using
     * the exportToSimulink(Curve) method.<br>
     * data has the following encoding:<br>
     * <pre>
     *  data[0][0] = Length of aperiodic part
     *  data[0][1] = Length of periodic part
     *  data[0][2] = period    
     *  data[1][0] = px0
     *  data[1][1] = py0
     *  data[1][2] = pdy
     *  data[2][0] = pds
     *  data[2][1] = pyMin
     *  data[2][2] = pyMax
     *  ...
     *  data[i][0] = segment.x
     *  data[i][1] = segment.y
     *  data[i][2] = segment.s
     *  ...
     * </pre>
     * 
	 * @param data a double[][] containiing the curve data.
	 * @return a Curve created from data.
	 */
	public static Curve importFromSimulink(double[][] data) {
        int aperLen  = (int)Math.round(data[0][0]);
        int perLen   = (int)Math.round(data[0][1]);
        long period  = Math.round(data[0][2]);
        double px0   = data[1][0];
        double py0   = data[1][1];
        double pdx   = (double)period;
        double pdy   = data[1][2];
        double pds   = data[2][0];
        double pyMin = data[2][1];
        double pyMax = data[2][2];
        SegmentList aper = null;
        SegmentList per  = null;
        int delta = 3;
        if (aperLen > 0) {
            aper = new SegmentList(aperLen);
            for (int i = delta; i < aperLen + delta; i++) {
                aper.add(new Segment(data[i][0], data[i][1], data[i][2]));
            }
            delta = delta + aperLen;
        }
        if (perLen > 0) {
            per = new SegmentList(perLen);
            for (int i = delta; i < perLen + delta; i++) {
                per.add(new Segment(data[i][0], data[i][1], data[i][2]));
            }
        }
        return new Curve(aper, per, px0, py0, period, pdx, pdy, pds, pyMin, pyMax, "");
    }
    
    /**
     * Exports a Curve to a double[][] representation.<br>
     * The returned data has the following encoding:<br>
     * <pre>
     *  data[0][0] = Length of aperiodic part
     *  data[0][1] = Length of periodic part
     *  data[0][2] = period    
     *  data[1][0] = px0
     *  data[1][1] = py0
     *  data[1][2] = pdy
     *  data[2][0] = pds
     *  data[2][1] = pyMin
     *  data[2][2] = pyMax
     *  ...
     *  data[i][0] = segment.x
     *  data[i][1] = segment.y
     *  data[i][2] = segment.s
     *  ...
     * </pre>
     * 
     * @param c the Curve to export
     * @return a double[][] encoding the Curve data.
     */
    public static double[][] exportToSimulink(Curve c) {
        int aperLen = 0;
        int perLen  = 0;
        if (c.hasAperiodicPart()) {
            aperLen = c.aperiodicSegments().size();
        }
        if (c.hasPeriodicPart()) {
            perLen = c.periodicSegments().size();
        }
        if (SIMULINK_DATA_LEN > aperLen + perLen + 3) {
            double[][] data = new double[SIMULINK_DATA_LEN][3];
            int delta = 3;
            SegmentList segList = null;
            Segment seg = null;
            if (c.hasAperiodicPart()) {
                segList = c.aperiodicSegments();
                for (int i = delta; i < aperLen + delta; i++) {
                    seg = segList.segment(i-delta);
                    data[i][0] = seg.x();
                    data[i][1] = seg.y();
                    data[i][2] = seg.s();
                }
                delta = delta + aperLen;
            }
            if (c.hasPeriodicPart()) {
                segList = c.periodicSegments();
                for (int i = delta; i < perLen + delta; i++) {
                    seg = segList.segment(i-delta);
                    data[i][0] = seg.x();
                    data[i][1] = seg.y();
                    data[i][2] = seg.s();
                }
                data[0][2] = c.period();
                data[1][0] = c.px0();
                data[1][1] = c.py0();
                data[1][2] = c.pdy();
                data[2][0] = c.pds();
                data[2][1] = c.pyMin();
                data[2][2] = c.pyMax();
            } else {
                data[0][2] = 0.0;
                data[1][0] = 0.0;
                data[1][1] = 0.0;
                data[1][2] = 0.0;
                data[2][0] = 0.0;
                data[2][1] = 0.0;
                data[2][2] = 0.0;
            }
            data[0][0] = aperLen;
            data[0][1] = perLen;
            return data;
        } else {
            throw new IndexOutOfBoundsException(Messages.SIMULINK_BUFFER_OVERFLOW 
                    + (aperLen + perLen + 3));
        }
    }

    /**
     * Sets the length of the double[][] that is used to represent a Curve in
     * Simulink. Curves are represented in Simulink using an double array of 
     * size double[len][3]
     * 
     * @param len
     */
    public static void setSimulinkDataLength(int len) {
        SIMULINK_DATA_LEN = len;
    }
    
    /**
     * Returns the length of the double[][] that is used to represent a Curve
     * in Simulink.
     * 
     * @return the length of the double[][] that is used to represent a Curve
     * in Simulink.
     */
    public static int getSimulinkDataLength() {
        return SIMULINK_DATA_LEN;
    }
    
    private static int SIMULINK_DATA_LEN = 250;
}
